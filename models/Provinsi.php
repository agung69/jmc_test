<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Provinsi extends ActiveRecord
{
  public function rules()
  {
    return [
      [['nama_provinsi'], 'required'],
      ['nama_provinsi', 'string', 'max'=>50],
    ];
  }

  public function getKabupatens()
  {
    return $this->hasMany(Kabupaten::className(), ['id_provinsi' => 'id']);
  }
}