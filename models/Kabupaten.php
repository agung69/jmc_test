<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Kabupaten extends ActiveRecord
{
  public function rules()
  {
    return [
      [['id_provinsi', 'nama_kabupaten', 'jumlah_penduduk'], 'required'],
      ['nama_kabupaten', 'string', 'max'=>50],
    ];
  }

  public function getProvinsi()
  {
      return $this->hasOne(Provinsi::className(), ['id' => 'id_provinsi']);
  }
}