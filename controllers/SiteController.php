<?php

namespace app\controllers;

use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Provinsi;
use app\models\Kabupaten;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionProvinsi()
    {
        $model = new Provinsi();

        $dataProvinsi2 = Provinsi::find()
                                ->select('provinsi.*, SUM(kabupaten.jumlah_penduduk) AS jumlah')
                                ->joinWith('kabupatens')
                                ->groupBy('provinsi.id')
                                ->all();

        $dbCommand = Yii::$app->db->createCommand('SELECT provinsi.*, SUM(kabupaten.jumlah_penduduk) AS jumlah FROM provinsi LEFT JOIN kabupaten ON provinsi.id = kabupaten.id_provinsi GROUP BY provinsi.id');

        $dataProvinsi = $dbCommand->queryAll();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if(Yii::$app->request->post('Provinsi')['id']){
                $model = Provinsi::findOne(Yii::$app->request->post('Provinsi')['id']);
                $model->nama_provinsi = Yii::$app->request->post('Provinsi')['nama_provinsi'];
            }

            if($model->save()){
                Yii::$app->session->setFlash('success', 'Data Berhasil Disimpan');
                return $this->refresh();
            }
        }

        return $this->render('provinsi', compact('model','dataProvinsi'));
    }

    public function actionProvinsiDelete()
    {
        $id = Yii::$app->request->get(1)['id'];
        $model = Provinsi::findOne($id);
        if($model->delete()){
            Yii::$app->session->setFlash('success', 'Data Berhasil Dihapus');
            $this->redirect(array('/site/provinsi'));
        }


    }

    public function actionKabupaten()
    {
        $model = new Kabupaten();

        $dataProvinsi = Provinsi::find()->all();

        $where = '';
        if(Yii::$app->request->get(1))
        {
            $where = ' WHERE id_provinsi = '.Yii::$app->request->get(1)['id_provinsi'];
        }

        $dbCommand = Yii::$app->db->createCommand("SELECT kabupaten.*, provinsi.nama_provinsi FROM kabupaten LEFT JOIN provinsi ON provinsi.id = kabupaten.id_provinsi".$where);
        
        $dataKabupaten = $dbCommand->queryAll();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if(Yii::$app->request->post('Kabupaten')['id']){
                $model = Kabupaten::findOne(Yii::$app->request->post('Kabupaten')['id']);
                $model->nama_kabupaten = Yii::$app->request->post('Kabupaten')['nama_kabupaten'];
                $model->jumlah_penduduk = Yii::$app->request->post('Kabupaten')['jumlah_penduduk'];
                $model->id_provinsi = Yii::$app->request->post('Kabupaten')['id_provinsi'];
            }

            if($model->save()){
                Yii::$app->session->setFlash('success', 'Data Berhasil Disimpan');
                return $this->refresh();
            }
        }

        return $this->render('kabupaten', compact('model', 'dataProvinsi','dataKabupaten'));
    }

    public function actionKabupatenDelete()
    {
        $id = Yii::$app->request->get(1)['id'];
        $model = Kabupaten::findOne($id);
        if($model->delete()){
            Yii::$app->session->setFlash('success', 'Data Berhasil Dihapus');
            $this->redirect(array('/site/kabupaten'));
        }


    }
}
