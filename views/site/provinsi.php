<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Provinsi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-provinsi">
    <h1><?= Html::encode($this->title) ?>
    <?= Html::button('New', ['class' => 'btn btn-primary', 'name' => 'reset-button', 'onclick' => 'reset()']) ?>
    </h1>
    
      <p>
          Please fill out the following form provinsi.
          Thank you.
      </p>

      <div class="row">
          <div class="col-lg-5">

              <?php $form = ActiveForm::begin([ 'method' => 'post',
                                                'action' => Url::to(['site/provinsi'])
                                              ]); ?>

                  <input type="hidden" id="provinsi-id" class="form-control" name="Provinsi[id]">
                  <?= $form->field($model, 'nama_provinsi')->textInput(['autofocus' => true]) ?>

                  <div class="form-group">
                      <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'provinsi-button']) ?>
                  </div>

              <?php ActiveForm::end(); ?>

          </div >

          <div class="col-lg-7">
            <table class="table table-bordered table-hover">
              <t-head>
                <tr class="success">
                  <th>Nama Provinsi</th>
                  <th>Jumlah Penduduk</th>
                  <th>Action</th>
                </tr>
              </t-head>
              <tbody>
              <?php foreach($dataProvinsi as $row): ?>
                <tr id="header-<?php echo $row['id'];?>">
                  <td class="nama_provinsi"><?php echo $row['nama_provinsi'];?></td>
                  <td><?php echo (int)$row['jumlah'];?></td>
                  <td>
                    <a class="btn btn-success btn-sm" onclick="edit(<?php echo $row['id'];?>)">edit</a>
                    <a href="<?php echo Url::to(['site/provinsi-delete', ['id' => $row['id']]]);?>" class="btn btn-danger btn-sm" onclick="delete(<?php echo $row['id'];?>)">delete</a>
                  </td>
                </tr>
              <?php endforeach;?>
              </tbody>

            </table>
          </div>
      </div>

</div>

<script>
  function edit(id){
    var nama_provinsi = $("#header-"+id+ "> .nama_provinsi" ).text();

    $("#provinsi-id").val(id);
    $("#provinsi-nama_provinsi").val(nama_provinsi);
  }
  function reset(){
    $("#provinsi-id").val('');
    $("#provinsi-nama_provinsi").val('');
  }
</script>