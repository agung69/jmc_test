<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use app\models\Provinsi;

$this->title = 'Kabupaten';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-kabupaten">
    <h1>
    <?= Html::encode($this->title) ?>
    <div class="btn-group">
      <?= Html::button('New', ['class' => 'btn btn-primary', 'name' => 'reset-button', 'onclick' => 'reset()']) ?>
      
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Provinsi <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <?php foreach($dataProvinsi as $row): ?>
          <li><a href="<?php echo Url::to(['site/kabupaten', ['id_provinsi' => $row['id']]]);?>"><?php echo $row['nama_provinsi'];?></a></li>
        <?php endforeach;?>
      </ul>
      
    </div>
    </h1>
    
      <p>
          Please fill out the following form kabupaten.
          Thank you.
      </p>

      <div class="row">
          <div class="col-lg-5">

              <?php $form = ActiveForm::begin([ 'method' => 'post',
                                                'action' => Url::to(['site/kabupaten'])
                                              ]); ?>

                  <input type="hidden" id="kabupaten-id" class="form-control" name="Kabupaten[id]">
                  <?= $form->field($model, 'nama_kabupaten')->textInput(['autofocus' => true]) ?>
                  <?= $form->field($model, 'jumlah_penduduk')->textInput(['type' => 'number']) ?>
                  <?= $form->field($model, 'id_provinsi')->dropDownList(ArrayHelper::map(Provinsi::find()->all(), 'id', 'nama_provinsi'),['class' => 'form-control']) ?>

                  <div class="form-group">
                      <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'kabupaten-button']) ?>
                  </div>

              <?php ActiveForm::end(); ?>

          </div >

          <div class="col-lg-7">
            <table class="table table-bordered table-hover">
              <t-head>
                <tr class="success">
                  <th>Nama Provinsi</th>
                  <th>Nama Kabupaten</th>
                  <th>Jumlah Penduduk</th>
                  <th>Action</th>
                </tr>
              </t-head>
              <tbody>
              <?php foreach($dataKabupaten as $row): ?>
                <tr id="header-<?php echo $row['id'];?>">
                  <input type="hidden" class="id_provinsi" value="<?php echo $row['id_provinsi'];?>">
                  <td class="nama_provinsi"><?php echo $row['nama_provinsi'];?></td>
                  <td class="nama_kabupaten"><?php echo $row['nama_kabupaten'];?></td>
                  <td class="jumlah_penduduk"><?php echo (int)$row['jumlah_penduduk'];?></td>
                  <td>
                    <a class="btn btn-success btn-sm" onclick="edit(<?php echo $row['id'];?>)">edit</a>
                    <a href="<?php echo Url::to(['site/kabupaten-delete', ['id' => $row['id']]]);?>" class="btn btn-danger btn-sm" onclick="delete(<?php echo $row['id'];?>)">delete</a>
                  </td>
                </tr>
              <?php endforeach;?>
              </tbody>

            </table>
          </div>
      </div>

</div>

<script>
  function edit(id){
    var nama_kabupaten = $("#header-"+id+ "> .nama_kabupaten" ).text();
    var jumlah_penduduk = $("#header-"+id+ "> .jumlah_penduduk" ).text();
    var id_provinsi = $("#header-"+id+ "> .id_provinsi" ).val();

    $("#kabupaten-id").val(id);
    $("#kabupaten-nama_kabupaten").val(nama_kabupaten);
    $("#kabupaten-jumlah_penduduk").val(jumlah_penduduk);
    $("#kabupaten-id_provinsi").val(id_provinsi);
  }
  function reset(){
    $("#kabupaten-id").val('');
    $("#kabupaten-nama_kabupaten").val('');
    $("#kabupaten-jumlah_penduduk").val('');
    $("#kabupaten-id_provinsi").val('');
  }
</script>